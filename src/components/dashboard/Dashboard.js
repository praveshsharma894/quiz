import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import questions from '../questions';
import Answers from './Answers';

const Dashboard = (props) => {

    let [category, setCategory] = useState('')
    let [question, setQuestion] = useState(null);
    let [count, setCount] = useState(0)
    let [score, setScore] = useState(0)
    let [isAnswered, setAnswered] = useState(false)
    let [showButton, setShowButton] = useState(false)
    let [showSelect, setShowSelect] = useState(true)


    const handleChange = (event) => {
        event.preventDefault()
        let { value } = event.target
        if (value !== "none") {
            setCategory(value)
            setQuestion(questions[value][count])
            setCount(c => c + 1)
            setShowSelect(false)
        }
    }

    const handleIncreaseScore = () => {
        setScore(c => c + 1)
    }

    const handleShowButton = () => {
        setAnswered(true)
        setShowButton(true)
    }

    const nextQuestion = () => {

        if (count === questions[category].length) {
        } else {
            insertData();
            setShowButton(false)
            setAnswered(false)
        }
    }

    const insertData = () => {
        setQuestion(questions[category][count])
        setCount(c => c + 1)
    }

    const logOut = () => {
        localStorage.removeItem('isLoggedIn');
        props.history.push('/')
    }

    if (questions[category] && count === questions[category].length) {
        return (
            <Redirect to={{
                pathname:"/result",
                state: { 'score': score, 'total': questions[category].length}
            }} />
        )
    }
    return (
        <div className="container">
        <div className="row header-row">
        <div>
        {showSelect &&<select
            value={category}
            onChange={handleChange}
        >
            <option value="none">Select an Option</option>
            <option value="react">React JS</option>
            <option value="python">Python</option>
        </select>}
        </div>
        <div>
        <button onClick={logOut}>Log Out</button>
        </div>
        </div>
            { question && <div className="row">
                <div className="col-lg-12 col-md-10">
                    <div id="question">
                        <h4 className="bg-light">Question {count}/{questions[category].length}</h4>
                        <p>{question.question}</p>
                    </div>

                    <Answers
                        answers={question.answers}
                        correct={question.correct}
                        showButton={handleShowButton}
                        isAnswered={isAnswered}
                        increaseScore={handleIncreaseScore}
                    />


                    <div id="submit">
                        {showButton ?
                            <button className="fancy-btn"
                                onClick={nextQuestion} >
                                {count === questions[category].length ? 'Finish quiz' : 'Next question'}
                            </button> : <span></span>}
                    </div>
                </div>
            </div>}
        </div>
    )
}

export default Dashboard;