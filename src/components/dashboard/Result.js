import React from 'react'

import { Link } from 'react-router-dom'

const Result = (props) => {
    let { score, total } = props.location.state
    return (
        <div>
            <p> You have Scored {score} out of {total} </p>
            <Link to="/home"> Home </Link>
        </div>
    )
}


export default Result