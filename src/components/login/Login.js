import React,{useState} from 'react';
import { Link, Redirect } from 'react-router-dom';

const Login = (props) => {

    let [name,setName] = useState('')
    let [password,setPassword] = useState('')

    const handleInput = (event) => {
        event.preventDefault()
        let {id:fieldId, value} = event.target;

        switch (fieldId) {
            case 'name': setName(value);
            break;
            case 'password': setPassword(value);
            break;
            default: return 0
        }
    }

    const submitData = (event) => {
        event.preventDefault()
        let user = JSON.parse(localStorage.getItem('users'))
        console.log(user)
        user.forEach(element => {
            console.log(element)
            console.log(password)
            if (element.name === name && element.password === password) {
                localStorage.setItem('isLoggedIn',true)
                props.history.push('/home')
            }
        });
    }

    return (
        !localStorage.getItem('isLoggedIn')?<div className="jumbotron">
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <h2>Login</h2>
                        <form onSubmit={submitData}>
                            <div className="form-group">
                                <label htmlFor="username">Username</label>
                                <input id="name" type="text" value={name} onChange={handleInput} className="form-control" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input id="password" value={password} onChange={handleInput} type="password" className="form-control" />
                            </div>
                            <div className="form-group">
                                <input type="submit" className="btn btn-primary" value="Login" />
                                <Link className="btn btn-link" to="/register" > Register </Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> : <Redirect to='/home' />
    )
}

export default Login