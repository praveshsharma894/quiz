export default {
    'react': [
        {
            question: 'We can go for keys when there is possibility that our user could change the data?',
            answers: [
                'Keys',
                'Ref',
                'Both',
                'None of above'
            ],
            correct: 1
        },

        {
            question: 'JSX is typesafe?',
            answers: [
                'True',
                'False',
                'None of the Above',
                'Cannot Be Determined'
            ],
            correct: 1
        },
        {
            question: 'React merges the object you provide into the current state using which of the following',
            answers: [
                'setState()',
                'State()',
                'getState()',
                'None of the Above'
            ],
            correct: 1
        },
        {
            question: 'Arbitrary inputs of components are called?',
            answers: [
                'Keys',
                'Props',
                'Elements',
                'Ref'
            ],
            correct: 2
        },
        {
            question: '_____ can be done while more than one element needs to be returned from a component?',
            answers: [
                'Abstraction',
                'Packing',
                'Insulation',
                'Wrapping'
            ],
            correct: 4
        },
        {
            question: 'Which of the following needs to be updated to achieve dynamic UI updates?',
            answers: [
                'State',
                'Props',
                'Components',
                'None of the Above'
            ],
            correct: 1
        },
        {
            question: 'Lifecycle methods are mainly used _____',
            answers: [
                'To keep track of event history',
                'To enhance components',
                'Free up resources',
                'None of the Above'
            ],
            correct: 3
        }
    ],
    'python': [
        {
            question: 'What is the maximum possible length of an identifier?',
            answers: [
                '16',
                '32',
                '64',
                'None of the Above'
            ],
            correct: 4
        },
        {
            question: 'Who developed the Python language?',
            answers: [
                'Zim Den',
                'Guido van Rossum',
                'Niene Stom',
                'Wick van Rossum'
            ],
            correct: 2
        },
        {
            question: 'Which of the following statements is correct regarding the object-oriented programming concept in Python?',
            answers: [
                'Classes are real-world entities while objects are not real',
                'Objects are real-world entities while classes are not real',
                'Both objects and classes are real-world entities',
                'All of the Above'
            ],
            correct: 2
        },
        {
            question: ' Which of the following is not a keyword in Python language?',
            answers: [
                'val',
                'raise',
                'try',
                'with'
            ],
            correct: 1
        },
        {
            question: 'Which of the following operators is the correct option for power(ab)?',
            answers: [
                'a^b',
                'a**b',
                'a^^b',
                'a^*b'
            ],
            correct: 2
        },
        {
            question: 'In which language is Python written?',
            answers: [
                'English',
                'PHP',
                'C',
                'All of the Above'
            ],
            correct: 3
        },
        {
            question: 'Which one of the following is the correct extension of the Python file?',
            answers: [
                '.py',
                '.python',
                '.p',
                'None of the Above'
            ],
            correct: 4
        }
    ]
}