import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const UserRegistration = (props) => {

    let [name, setName] = useState('');
    let [password, setPassword] = useState('');
    let [email, setEmail] = useState('')

    const submitData = (event) => {
        event.preventDefault();
        let user = JSON.parse(localStorage.getItem('users'))
        let newUser = {
            name,
            password,
            email
        }
        if (!user) {
            let userData = [newUser]
            localStorage.setItem('users', JSON.stringify(userData))
        } else {
            user.push(newUser)
            localStorage.setItem('users',JSON.stringify(user))
        }
        props.history.push('/')
    }

    const handleInput = (event) => {
        event.preventDefault();
        let {id:fieldId, value} = event.target;
        switch (fieldId) {
            case 'name': setName(value)
                break;
            case 'password': setPassword(value)
                break;
            case 'email': setEmail(value)
            break;
            default: return 0;
        }
    }

    return (
        <div className="jumbotron">
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 offset-sm-3">
                        <h2>Register</h2>
                        <form onSubmit={submitData}>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input id="name" type="text" value={name} onChange={handleInput} class="form-control" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input id="password" type="password" value={password} onChange={handleInput} class="form-control" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input id="email" type="email" value={email} onChange={handleInput} className="form-control" />
                            </div>
                            <div className="form-group">
                                <input type="submit" className="btn btn-primary" />
                                <Link to="/" className="btn btn-link">Cancel</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default UserRegistration;