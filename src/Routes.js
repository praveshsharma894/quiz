import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Login from './components/login/Login';
import UserRegistration from './components/register/UserRegistration';
import Dashboard from './components/dashboard/Dashboard';
import Result from './components/dashboard/Result';

const Routes = (props) => {
    return (
        <BrowserRouter>
            <React.Fragment>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route path="/register" component={UserRegistration} />
                    <Route path="/home" component={Dashboard} />
                    <Route path="/result" component={Result} />
                </Switch>
            </React.Fragment>
        </BrowserRouter>
    )
}

export default Routes;