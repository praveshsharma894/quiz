import logo from './logo.svg';
import './App.css';

import Routes from './Routes'

function App() {
  return (
    <div>
      <Routes></Routes>
    </div>
  );
}

export default App;
